<?php
/* Autor : Armando Barraza Rodriguez */
/* DevSecOps Codes : HackTrack */
echo "SAST Reporte HTML Convertidor\n";
$archivo = $argv[1];
$ext = pathinfo($archivo, PATHINFO_EXTENSION);

if ( $ext != "json") { echo 'formato de archivo no valido.'; exit(1); }
if ( !is_file(dirname(__FILE__)."/$archivo") ) { echo 'problema con archivo.'; exit(1); }
$contenido = file_get_contents($argv[1]);
$contenido_decode = json_decode($contenido);

$repositorio     = getenv("CI_PROJECT_URL");
$rama            = getenv("CI_COMMIT_BRANCH");
$url_repositorio = "$repositorio/-/tree/$rama/";

ob_start();
echo "<html>\n";
echo "<head>\n";
echo "<style>\n";
echo "html { background : black; color: white; width: 100%; }\n";
echo "table, th, td { border: 1px solid white; border-collapse: collapse; padding: 10px; }\n";
echo "table {  width: 80%; margin: auto; font-size: small; }\n";
echo 'body { font-family: "Arial"}';
echo "</style>\n";
echo "</head>\n";
echo "<body>\n";
echo "<table>\n";

echo "<tr>\n";
echo "<th style='background-color: #1f73bb;' colspan='6'>";
echo "Revisi&oacute;n Reporte de Vulnerabilidades";
echo "</th>\n";
echo "</tr>\n";
echo "<tr>\n";
echo "<th style='background-color: #b51010;'>";
echo "Categor&iacute;a";
echo "</th>\n";
echo "<th style='background-color: #b51010;'>";
echo "Fallo";
echo "</th>\n";
echo "<th style='background-color: #b51010;'>";
echo "Mensaje";
echo "</th>\n";
echo "<th style='background-color: #b51010;'>";
echo "Descripci&oacute;n";
echo "</th>\n";
echo "<th style='background-color: #b51010;'>";
echo "Severidad";
echo "</th>\n";
echo "<th style='background-color: #b51010;'>";
echo "Archivo Afectado";
echo "</th>\n";
echo "</tr>\n";
foreach( $contenido_decode->vulnerabilities as $vulnerabilidad ) {
    echo "<tr>\n";
    echo "<th>";
    echo ($vulnerabilidad->category);
    echo "</th>\n";
    echo "<th>";
    echo ($vulnerabilidad->name);
    echo "</th>\n";
    echo "<th>";
    echo ($vulnerabilidad->message);
    echo "</th>\n";
    echo "<th>";
    echo ($vulnerabilidad->description);
    echo "</th>\n";
    echo "<th>";
    echo ($vulnerabilidad->severity);
    echo "</th>\n";
    echo "<th>";
    echo "<a href='".$url_repositorio.$vulnerabilidad->location->file."#L".$vulnerabilidad->location->start_line."' target='_blank'>".$vulnerabilidad->location->file.":".$vulnerabilidad->location->start_line."</a>";
    echo "</th>";
    echo "</tr>\n";
}

echo "</table>\n";
echo "</body>\n";
echo "</html>\n";

$htmlReporte = ob_get_contents();
ob_end_clean();

$output = pathinfo($archivo, PATHINFO_FILENAME);
$output = "$output.html";
$archivoSalida = fopen($output, "w");
fwrite($archivoSalida, $htmlReporte);
fclose($archivoSalida);
?>